# Long text in array

Use display_rows to set text input as textarea inside Array of text.

![Screenshot](survey/questions/answer/arrays/texts/assets/arraytext_display_rows.png?raw=true "Screenshot of textarea in array texts")

## Installation and sage

1. Extract the download and upload the "arrayTextArea" folder to ./upload/themes/question/.
2. Create an Array (Texts) question
3. Set the question setting "Question theme" to "Long array text".

### Optionnal settings 

- _Display rows_ : number of row for textarea, default is 4.
- _Restricted to columns_ : Sub question code of X scale columns where textarea is set. Each sub question code must be separated by a comma (`,`). If empty : textearea is set for all columns.

## Copyright and home page

- HomePage <https://extensions.sondages.pro>
- Copyright © 2020-2022 Denis Chenu <https://sondages.pro>
- Licence : Expat/MIT <https://directory.fsf.org/wiki/License:Expat>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)
- [Donate via support ticket](https://support.sondages.pro/open.php?topicId=12)
- Code, issue and feature : [gitlab](https://gitlab.com/SondagesPro/QuestionTheme/arrayTextArea)
